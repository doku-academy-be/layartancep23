package com.example.moviesservice.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.time.Duration;
import java.util.Date;

@Entity
@Table(name = "movies")
@Data
public class MoviesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 100)
    private String title;
    private String genre;
    private String description;
    private Date ReleaseDate;
    private String Duration;

}
