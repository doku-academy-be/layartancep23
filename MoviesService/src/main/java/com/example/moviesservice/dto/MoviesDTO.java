package com.example.moviesservice.dto;

import jakarta.persistence.Column;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString

public class MoviesDTO implements Serializable {
    private Long id;
    private String title;
    private String genre;
    private String description;
    private Date ReleaseDate;
    private String Duration;
}
