package com.example.moviesservice.dto;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class PaginateMoviesDTO implements Serializable {
    private List<MoviesDTO> MoviesDTOList;
    private Long totalOfItems;
    private Integer currentPage;
    private Integer totalOfPages;

}
