package com.example.moviesservice.service.implementation;


import com.example.moviesservice.dto.MoviesDTO;
import com.example.moviesservice.dto.PaginateMoviesDTO;
import com.example.moviesservice.entity.MoviesEntity;
import com.example.moviesservice.exception.MoviesNotfoundException;
import com.example.moviesservice.repository.MoviesRepository;
import com.example.moviesservice.service.MovieService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;


@Slf4j
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@CacheConfig(cacheNames = {"movies"})

public class MovieServiceImp implements MovieService {
    private final MoviesRepository moviesRepository;
    private final ModelMapper modelMapper;

    @Override
    @Cacheable
    public List<MoviesDTO> getAllMovies() {
        List<MoviesEntity> moviesList = moviesRepository.findAll();
        List<MoviesDTO> moviesDTOList = new ArrayList<>();
        for (MoviesEntity movies : moviesList) {
            MoviesDTO lessonDTO = modelMapper.map(movies, MoviesDTO.class);
            moviesDTOList.add(lessonDTO);
        }
        log.info("Getting All Movies from databases");
        return moviesDTOList;
    }
    @Override
    public PaginateMoviesDTO getAllMoviesWithPagination(Pageable pageable) {
        Page<MoviesEntity> movies = moviesRepository.findAll(pageable);
        List<MoviesDTO> moviesDTOList = new ArrayList<>();
        for (MoviesEntity movie : movies.getContent()) {
            MoviesDTO moviesDTO = modelMapper.map(movie, MoviesDTO.class);
            moviesDTOList.add(moviesDTO);
        }
        return PaginateMoviesDTO.builder().MoviesDTOList(moviesDTOList).totalOfItems(movies.getTotalElements()).totalOfPages(movies.getTotalPages()).currentPage(movies.getNumber()).build();
    }
    @Override
    public MoviesDTO getMovieById(Long id) {
        Optional<MoviesEntity> moviesOptional = moviesRepository.findById(id);
        if (moviesOptional.isPresent()) {
            MoviesDTO moviesDTO = modelMapper.map(moviesOptional.get(), MoviesDTO.class);
            log.info("Getting Movie with id : {} from databases", id);
            return moviesDTO;
        }
        log.error("Movie with id : {} not found!", id, new MoviesNotfoundException("Movie not found"));
        throw new MoviesNotfoundException("Movie Not Found");
    }
    @Override
    @Transactional()
    @CacheEvict(value = "movies", allEntries = true)
    public void addMovie(MoviesDTO moviesDTO) {
        MoviesEntity moviesEntity = modelMapper.map(moviesDTO, MoviesEntity.class);
        moviesRepository.save(moviesEntity);
        log.info("Success add movie : {}", moviesDTO.toString());
    }
    @Override
    @Transactional()
    @CacheEvict(value = "movies", allEntries = true)
    public void updateMovie(MoviesDTO moviesDTO, Long id) {
        Optional<MoviesEntity> optionalMovies = moviesRepository.findById(id);

        if (optionalMovies.isEmpty()) {
            log.error("Movie with id : {} not found!", id, new MoviesNotfoundException("movie not found"));
            throw new MoviesNotfoundException("Movie Not Found");
        }

        MoviesEntity movies = optionalMovies.get();
        modelMapper.map(moviesDTO, movies);
        moviesRepository.save(movies);
        log.info("Success update movie : {}", movies.toString());
    }

    @Override
    @Transactional()
    @CacheEvict(value = "movies", allEntries = true)
    public void deleteMovie(Long id) {
        Optional<MoviesEntity> optionalMovies = moviesRepository.findById(id);

        if (optionalMovies.isEmpty()) {
            log.error("Movie with id : {} not found!", id, new MoviesNotfoundException("movie not found"));
            throw new MoviesNotfoundException("Movie Not Found");
        }
        MoviesEntity movies = optionalMovies.get();
        moviesRepository.delete(movies);
        log.info("success delete movie : {}", movies.toString());

    }


}
