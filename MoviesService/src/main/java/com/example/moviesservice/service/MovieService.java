package com.example.moviesservice.service;

import com.example.moviesservice.dto.MoviesDTO;
import com.example.moviesservice.dto.PaginateMoviesDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MovieService {
    List<MoviesDTO> getAllMovies();
    PaginateMoviesDTO getAllMoviesWithPagination(Pageable pageable);
    MoviesDTO getMovieById(Long id);
    void addMovie(MoviesDTO moviesDTO);
    void updateMovie(MoviesDTO moviesDTO, Long id);
    void deleteMovie(Long id);
//    void takeLesson(String token, Long id);
//    UserCertsDTO buildUserCertsDTO(String token, Long id);
//    Boolean isUserTakeLesson(Long userId, Long lessonId);

}
