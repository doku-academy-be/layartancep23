package com.example.moviesservice.repository;

import com.example.moviesservice.entity.MoviesEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoviesRepository extends JpaRepository<MoviesEntity,Long> {
    Page<MoviesEntity> pagination(Pageable pageable);
}
